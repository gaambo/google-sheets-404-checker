function onOpen(e) {
  SpreadsheetApp.getUi().createAddonMenu()
      .addItem('Check URLS', 'checkNewUrls')
      .addToUi();
}

function checkNewUrls() {
  
  /* TODO */
  //time out is a problem - just run it multiple times with overwrite false
  
  /* CONFIG */ 
  var urlsToCheckColumn = 2; 
  var overwrite = false;
  
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  var rows = sheet.getRange(2,urlsToCheckColumn,sheet.getLastRow(),2); 
  var rowValues = rows.getValues(); 
  
  rowValues.forEach(function(row,index) {
    if(!row[1] || overwrite) { //if not already has status
      var url = row[0]; 
      if(url) {
        var response = UrlFetchApp.fetch(url,{
          muteHttpExceptions: true,
          followRedirects: true
        }); 
        var responseCode = response.getResponseCode(); 
        row[1] = responseCode; 
      } else {
        var responseCode = '-'; 
      }
      
      sheet.getRange(index+2,3).setValue(responseCode); //+2 because zero-based-index and headline-row
    }  
    
  }); 
  
  //sheet.getRange(1,2,sheet.getLastRow(),2).setValues(rowValues); 
}